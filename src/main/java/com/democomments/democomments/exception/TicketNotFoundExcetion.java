package com.democomments.democomments.exception;

public class TicketNotFoundExcetion extends RuntimeException{
    private final String message;

    public TicketNotFoundExcetion(String message){
        this.message=message;
    }
    @Override
    public String getMessage(){
        return message;
    }
}
