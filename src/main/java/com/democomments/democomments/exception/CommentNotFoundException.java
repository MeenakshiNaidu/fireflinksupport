package com.democomments.democomments.exception;

public class CommentNotFoundException extends  RuntimeException{
    private final String message;

    public CommentNotFoundException(String message){
        this.message=message;
    }
    @Override
    public String getMessage(){
        return message;
    }
}
