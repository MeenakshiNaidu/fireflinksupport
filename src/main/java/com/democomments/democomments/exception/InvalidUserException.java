package com.democomments.democomments.exception;

public class InvalidUserException extends RuntimeException{
    private final String message;

    public InvalidUserException(String message){
        this.message=message;
    }
    @Override
    public String getMessage(){
        return message;
    }
}
