package com.democomments.democomments.exception;

import com.democomments.democomments.payload.ResponseStructure;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ApplicationHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(TicketNotFoundExcetion.class)
    public ResponseEntity<ResponseStructure<String>> ticketNotFoundExceptionHandler(TicketNotFoundExcetion exception){
        return new ResponseEntity<>(new ResponseStructure<>(HttpStatus.NOT_FOUND.value(), "Incorrect Ticket Id",exception.getMessage()),(HttpStatus.OK));
    }

    @ExceptionHandler(InvalidUserException.class)
    public ResponseEntity<ResponseStructure<String>> invalidUserExceptionHandler(InvalidUserException exception){
        return new ResponseEntity<>(new ResponseStructure<>(HttpStatus.NOT_FOUND.value(), "Invalid User to perform this operation",exception.getMessage()),(HttpStatus.OK));
    }

    @ExceptionHandler(CommentNotFoundException.class)
    public ResponseEntity<ResponseStructure<String>> commentNotFpundExceptionHandler(CommentNotFoundException exception){
        return new ResponseEntity<>(new ResponseStructure<>(HttpStatus.NOT_FOUND.value(), "Comment Not Found",exception.getMessage()),(HttpStatus.OK));
    }
}
