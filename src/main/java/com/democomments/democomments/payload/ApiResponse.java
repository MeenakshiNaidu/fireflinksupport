package com.democomments.democomments.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiResponse {
    private HttpStatus httpStatus;
    private int statusCode;
    private Object response;
}
