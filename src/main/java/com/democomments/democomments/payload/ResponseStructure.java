package com.democomments.democomments.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class ResponseStructure<T> {
    private int statusCode;
    private T data;
    private String message;
}
