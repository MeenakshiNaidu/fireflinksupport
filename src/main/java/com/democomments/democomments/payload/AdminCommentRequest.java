package com.democomments.democomments.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdminCommentRequest {
    private String replay;
    private Boolean isPrivate;
}
