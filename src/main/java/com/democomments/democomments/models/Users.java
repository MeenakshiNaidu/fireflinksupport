package com.democomments.democomments.models;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Users {
    @Id
    private String userId;
    private String name;
    private String email;
    private String password;
    private String role;
}
