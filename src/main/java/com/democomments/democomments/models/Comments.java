package com.democomments.democomments.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Comments extends BaseEntity {

    private String commentId;
    private String replay;
    private Boolean isPrivate=false;

}
