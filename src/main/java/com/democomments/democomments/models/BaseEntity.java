package com.democomments.democomments.models;

import com.democomments.democomments.util.GetTimeDate;
import lombok.Data;

@Data
public class BaseEntity {
    private String createdByEmail;
    private String createdByName;
    private String createdOn;
    private String updatedByName;
    private String updatedByEmail;
    private String updatedOn;

    public void createdEntity(String createdByEmail,String createdByName){
        this.createdByName=createdByName;
        this.createdByEmail=createdByEmail;
        this.createdOn= GetTimeDate.getCurrentTime();
    }
    public void updatedEntity(String updatedByEmail,String updatedByName){
        this.updatedByEmail=updatedByEmail;
        this.updatedByName=updatedByName;
        this.updatedOn=GetTimeDate.getCurrentTime();
    }

}
