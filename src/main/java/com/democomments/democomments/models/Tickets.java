package com.democomments.democomments.models;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Tickets {
    @Id
    private String ticketId;
    private String name;
    private String type;
    private List<Comments> comments;
    private List<String> accessEmails;
}
