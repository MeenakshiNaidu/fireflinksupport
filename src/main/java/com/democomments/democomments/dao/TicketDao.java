package com.democomments.democomments.dao;

import com.democomments.democomments.exception.TicketNotFoundExcetion;
import com.democomments.democomments.models.Tickets;
import com.democomments.democomments.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TicketDao {
//    @Autowired
//    private TicketRepository ticketRepository;
    @Autowired
     private MongoTemplate mongoTemplate;
    private final TicketRepository ticketRepository;

    public TicketDao(TicketRepository ticketRepository){
        this.ticketRepository=ticketRepository;
    }

    public Tickets saveTickets(Tickets ticket){
       return ticketRepository.save(ticket);
    }

    public Tickets getTicketById(String ticketId){

        return ticketRepository.findById(ticketId).orElseThrow(()->new TicketNotFoundExcetion("Tick not found"));
    }

    public List<Tickets> searchTickets(String ticketName){
        Query query=Query.query(Criteria.where("name").regex(ticketName));
        return mongoTemplate.find(query,Tickets.class,"tickets");

    }

    public List<Tickets> FindAllTickets() {
        return ticketRepository.findAll();
    }
}
