package com.democomments.democomments.dao;

import com.democomments.democomments.models.Users;
import com.democomments.democomments.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserDao {
    @Autowired
    private UserRepository  userRepository;
    public Users saveUser(Users user){
        return userRepository.save(user);
    }


}
