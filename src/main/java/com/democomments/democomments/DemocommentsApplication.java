package com.democomments.democomments;

import com.democomments.democomments.dao.UserDao;
import com.democomments.democomments.models.Comments;
import com.democomments.democomments.models.Users;
import com.democomments.democomments.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemocommentsApplication implements CommandLineRunner {
	@Autowired
	private UserDao userRepository;
	public static void main(String[] args) {
		SpringApplication.run(DemocommentsApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		for (int i=0;i<10;i++){
			Users user=new Users();
			user.setUserId("FFE"+i);
			user.setEmail("ffe"+i+"@gmail.com");
			user.setPassword(i+"a");
			user.setRole("customer");
			user.setName("kiran");
			userRepository.saveUser(user);
		}

	}
}
