package com.democomments.democomments.controller;

import com.democomments.democomments.models.Tickets;
import com.democomments.democomments.payload.AdminCommentRequest;
import com.democomments.democomments.payload.ApiResponse;
import com.democomments.democomments.payload.ResponseStructure;

import com.democomments.democomments.service.TicketService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TicketController {
//    @Autowired
//    private TicketService ticketService;

    private final TicketService ticketService;

    public TicketController(TicketService ticketService){
        this.ticketService=ticketService;
    }

    @PostMapping("/ticket")
    public ResponseEntity<ResponseStructure<Tickets>> saveTicket(@RequestBody Tickets tickets){
        return ticketService.saveTicket(tickets);
    }

//    @PutMapping("/{ticketId}/{userEmail}")
//    public ResponseEntity<ResponseStructure<Tickets>> updateTicketComments(@PathVariable String ticketId, @PathVariable String userEmail, @RequestBody CommentRequest commentRequest){
//        return ticketService.updateTicketComments(ticketId,userEmail,commentRequest);
//    }

    @PutMapping("/admin/{ticketId}/{userEmail}")
    public ResponseEntity<ApiResponse> updateAdminTicketComments(@PathVariable String ticketId, @PathVariable String userEmail, @RequestBody AdminCommentRequest commentRequest){
        return ticketService.addTicketComments(ticketId,userEmail,commentRequest);
    }

    @PutMapping("/{ticketId}/{commentId}/{userEmail}")
    public ResponseEntity<ApiResponse> updateCommment(@PathVariable String ticketId,@PathVariable String commentId,@PathVariable String userEmail,@RequestBody AdminCommentRequest commentRequest){
        return ticketService.updateComment(ticketId,commentId,userEmail,commentRequest);
    }

    @DeleteMapping("/{ticketId}/{commentId}")
    public ResponseEntity<ApiResponse> deleteCommment(@PathVariable String ticketId,@PathVariable String commentId,@RequestParam String userEmail){
        return ticketService.deleteComment(ticketId,commentId,userEmail);
    }

    @GetMapping("/tickets/{ticketName}")
    public ResponseEntity<ApiResponse> searchTickets(@PathVariable String ticketName){
        return ticketService.searchTiskets(ticketName);
    }

    @GetMapping("/comment/{ticketId}/{commentId}/{email}")
    public ResponseEntity<ApiResponse> findCommentById(@PathVariable String ticketId,@PathVariable String commentId,@PathVariable String email){
        return ticketService.getCommmentById(ticketId,commentId,email);
    }

//    @GetMapping("/admin/comment/{ticketId}/{commentId}/{email}")
//    public ResponseEntity<ResponseStructure<Comments>> findAdminCommentById(@PathVariable String ticketId,@PathVariable String commentId,@PathVariable String email){
//        return ticketService.getAdminCommmentById(ticketId,commentId,email);
//    }
    @GetMapping("/comments/{ticketId}/{userEmail}")
    public ResponseEntity<ApiResponse> getAllComments(@PathVariable String ticketId,@PathVariable String userEmail){
        return ticketService.getAllCommments(ticketId,userEmail);
    }


}
