package com.democomments.democomments.service;

import com.democomments.democomments.models.Tickets;
import com.democomments.democomments.payload.AdminCommentRequest;
import com.democomments.democomments.payload.ApiResponse;
import com.democomments.democomments.payload.ResponseStructure;
import org.springframework.http.ResponseEntity;

public interface TicketService {

    ResponseEntity<ResponseStructure<Tickets>> saveTicket(Tickets ticket);
    ResponseEntity<ApiResponse> addTicketComments(String ticketId, String userEmail, AdminCommentRequest commentRequest);
    ResponseEntity<ApiResponse> updateComment(String ticketId,String commentId,String userEmail,AdminCommentRequest commentRequest);
    ResponseEntity<ApiResponse> deleteComment(String ticketId,String commentId,String email);
    ResponseEntity<ApiResponse> searchTiskets(String ticketName);
    ResponseEntity<ApiResponse> getCommmentById(String ticketId,String commentId,String email);
    ResponseEntity<ApiResponse> getAllCommments(String ticketid,String userEmail);



    }
