package com.democomments.democomments.service;

import com.democomments.democomments.dao.TicketDao;
import com.democomments.democomments.exception.CommentNotFoundException;
import com.democomments.democomments.models.Comments;
import com.democomments.democomments.models.Tickets;
import com.democomments.democomments.models.Users;
import com.democomments.democomments.payload.AdminCommentRequest;
import com.democomments.democomments.payload.ApiResponse;
import com.democomments.democomments.payload.ResponseStructure;
import com.democomments.democomments.util.ResponseStructureUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class TicketServiceimpl implements TicketService{
//    @Autowired
//    private TicketDao ticketDao;


    private final TicketDao ticketDao;

    public TicketServiceimpl(TicketDao ticketDao){

        this.ticketDao=ticketDao;
    }
    private static final String ADMIN_EMAIL="admin@gmail.com";
    @Autowired
    private  MongoTemplate mongoTemplate;
    @Override
    public ResponseEntity<ResponseStructure<Tickets>> saveTicket(Tickets ticket){
        if(ticket.getAccessEmails()!=null){
            ticket.getAccessEmails().add(ADMIN_EMAIL);
        }
        else{
            List<String> emails=new ArrayList<>();
            emails.add(ADMIN_EMAIL);
            ticket.setAccessEmails(emails);
        }
       return new ResponseEntity<>(new ResponseStructure<>(HttpStatus.CREATED.value(), ticketDao.saveTickets(ticket), "Ticket Created Sucessfullly"), (HttpStatus.CREATED));
    }

    public ResponseEntity<ResponseStructure<Tickets>> getTicketById(String ticketId){
        return new ResponseEntity<>(new ResponseStructure<>(HttpStatus.OK.value(),ticketDao.getTicketById(ticketId),"Ticket Found"),(HttpStatus.OK));
    }

//    public ResponseEntity<ResponseStructure<Tickets>> updateTicketComments(String ticketId, String userEmail, CommentRequest commentRequest){
//        Tickets ticket=ticketDao.getTicketById(ticketId);
//        if(ticket.getAccessEmails().contains(userEmail)) {
//            Comments crestedComment = new Comments();
//            crestedComment.setCommentId(UUID.randomUUID().toString());
//            crestedComment.setReplay(commentRequest.getReplay());
//            crestedComment.setCreatedEmail(userEmail);
//            crestedComment.setIsPrivate(false);
//            crestedComment.setCreatedTime(LocalDateTime.now());
//            if (ticket.getComments() != null) {
//                ticket.getComments().add(crestedComment);
//                ticketDao.saveTickets(ticket);
//
//
//            } else {
//                List<Comments> comments = new ArrayList<>();
//                comments.add(crestedComment);
//                ticket.setComments(comments);
//                ticketDao.saveTickets(ticket);
//            }
//            return new ResponseEntity<>(new ResponseStructure<>(HttpStatus.OK.value(), ticket, "Comments added"), (HttpStatus.OK));
//        }
//        throw new InvalidUserException("You are not allowed to comment on this ticket");
//    }

    //adding comment
    @Override
    public ResponseEntity<ApiResponse> addTicketComments(String ticketId, String userEmail, AdminCommentRequest commentRequest){
        Tickets ticket=ticketDao.getTicketById(ticketId);
        Query query=Query.query(Criteria.where("email").is(userEmail));
        Users user=mongoTemplate.findOne(query, Users.class,"users");
        if(user!=null) {
            Comments crestedComment = new Comments();
            crestedComment.setCommentId(UUID.randomUUID().toString());
            crestedComment.setReplay(commentRequest.getReplay());
            //crestedComment.createdEntity(userEmail,user.getName());
            crestedComment.createdEntity(userEmail,user.getName());
           if(commentRequest.getIsPrivate()!=null){
               crestedComment.setIsPrivate(commentRequest.getIsPrivate());
           }

            if (ticket.getComments() != null) {
                ticket.getComments().add(crestedComment);
                ticketDao.saveTickets(ticket);

            } else {
                List<Comments> comments = new ArrayList<>();
                comments.add(crestedComment);
                ticket.setComments(comments);
                ticketDao.saveTickets(ticket);
            }
            return ResponseStructureUtil.getCreatedResponse(ticket);
//            return new ResponseEntity<>(new ResponseStructure<>(HttpStatus.OK.value(), ticket, "Comments added"), (HttpStatus.OK));
        }
        return ResponseStructureUtil.getNotFoundResponse("User Not Found");

    }
    @Override
    public ResponseEntity<ApiResponse> updateComment(String ticketId,String commentId,String userEmail,AdminCommentRequest commentRequest){
        Tickets ticket=ticketDao.getTicketById(ticketId);
        Comments comment=ticket.getComments().stream().filter(Id->Id.getCommentId().equals(commentId)).findFirst().orElseThrow(()->new CommentNotFoundException("No comment preasnt"));
        Query query=Query.query(Criteria.where("email").is(userEmail));
        Users user=mongoTemplate.findOne(query, Users.class,"users" );
       if(user!=null){
           if((comment.getCreatedByEmail().equals(userEmail)||(user.getRole().equals("admin")))){
               if(commentRequest.getReplay()!=null){
                   comment.setReplay(commentRequest.getReplay());
               }
               if(commentRequest.getIsPrivate()!=null){
                   comment.setIsPrivate(commentRequest.getIsPrivate());
               }
               comment.updatedEntity(userEmail,user.getName());
               ticketDao.saveTickets(ticket);
               return ResponseStructureUtil.getOkResponse(ticket);
//           return new ResponseEntity<>(new ResponseStructure<>(HttpStatus.OK.value(),ticket,"Commment Updated"),(HttpStatus.OK));
           }
           return ResponseStructureUtil.getOkResponse("Unauthorised User");
       }
       return ResponseStructureUtil.getNotFoundResponse("User not found with given email");

    }
    @Override
    public ResponseEntity<ApiResponse> deleteComment(String ticketId,String commentId,String email){
        Tickets ticket=ticketDao.getTicketById(ticketId);
        Comments comment=ticket.getComments().stream().filter(Id->Id.getCommentId().equals(commentId)).findFirst().orElseThrow(()->new CommentNotFoundException("No comment preasnt"));
        Query query=Query.query(Criteria.where("email").is(email));
        Users user=mongoTemplate.findOne(query, Users.class,"user");
       if(user!=null){
           if(comment.getCreatedByEmail().equals(email) || user.getRole().equals("admin")){
               ticket.getComments().remove(comment);
               ticketDao.saveTickets(ticket);
               return ResponseStructureUtil.getOkResponse(ticket);
//            return new ResponseEntity<>(new ResponseStructure<>(HttpStatus.OK.value(),ticket,"Commment deleted"),(HttpStatus.OK));
           }
           return ResponseStructureUtil.getOkResponse("Unauthorised User");
       }
        return ResponseStructureUtil.getNotFoundResponse("User Not Found");
    }
    @Override
    public ResponseEntity<ApiResponse> searchTiskets(String ticketName){
        List<Tickets> foundTiskets=ticketDao.searchTickets(ticketName);
        return ResponseStructureUtil.getOkResponse(foundTiskets);
//        return new ResponseEntity<>(new ResponseStructure<>(HttpStatus.OK.value(), foundTiskets,"Available tickets are"),(HttpStatus.OK));
    }

    //method to get comment by id
    @Override
    public ResponseEntity<ApiResponse> getCommmentById(String ticketId,String commentId,String email){
        Tickets ticket=ticketDao.getTicketById(ticketId);
        Comments comment=ticket.getComments().stream().filter(comm->comm.getCommentId().equals(commentId)).findFirst().orElseThrow(()->new CommentNotFoundException("Comment not found"));
        Query query=Query.query(Criteria.where("email").is(email));
        Users user=mongoTemplate.findOne(query, Users.class,"users" );
       if(user!=null){
               if(!comment.getIsPrivate()){
                   return ResponseStructureUtil.getOkResponse(comment);
//                return new ResponseEntity<>(new ResponseStructure<>(HttpStatus.OK.value(),comment, "Commengt is"),(HttpStatus.OK));
               }
               else {
                   if(!user.getRole().equalsIgnoreCase("customer")){
//                  Comments comment=ticket.getComments().stream().filter(comm->comm.getCommentId().equals(commentId)).findFirst().orElseThrow(()->new CommentNotFoundException("Comment npt found"));
//                    return new ResponseEntity<>(new ResponseStructure<>(HttpStatus.OK.value(),comment,"Comment found" ),(HttpStatus.OK));
                       return ResponseStructureUtil.getOkResponse(comment);
                   }
                   return ResponseStructureUtil.getUnauthorisedResponse("Unauthorised User");

               }


       }
        return ResponseStructureUtil.getNotFoundResponse("User Not Found");

    }
    //method to get comment by id created by admin/support team it can visable to all abort from user
//    public ResponseEntity<ResponseStructure<Comments>> getAdminCommmentById(String ticketId,String commentId,String email){
//        Tickets ticket=ticketDao.getTicketById(ticketId);
//        Query query=Query.query(Criteria.where("email").is(email));
//        Users user=mongoTemplate.findOne(query, Users.class,"users" );
//        if(user!=null){
//            if(!user.getRole().equalsIgnoreCase("customer") && ticket.getAccessEmails().contains(email)){
//                Comments comment=ticket.getComments().stream().filter(comm->comm.getCommentId().equals(commentId)).findFirst().orElseThrow(()->new CommentNotFoundException("Comment npt found"));
//                return new ResponseEntity<>(new ResponseStructure<>(HttpStatus.OK.value(),comment,"Comment found" ),(HttpStatus.OK));
//            }
//            throw  new InvalidUserException("Your are not allowed to see this comment");
//        }
//        throw  new InvalidUserException("Your are not allowed to see this comment");
//    }
    @Override
    public ResponseEntity<ApiResponse> getAllCommments(String ticketid,String userEmail){
        Tickets ticket=ticketDao.getTicketById(ticketid);
        Query query=Query.query(Criteria.where("email").is(userEmail));
        Users user=mongoTemplate.findOne(query, Users.class,"users" );
            if(user!=null){
                if(!user.getRole().equalsIgnoreCase("customer")){
                    List<Comments> nonUserComments=ticket.getComments();
                    return ResponseStructureUtil.getOkResponse(nonUserComments);

//                return new ResponseEntity<>(new ResponseStructure<>(HttpStatus.OK.value(), nonUsertComments,"comments of this ticket is"),(HttpStatus.OK));
                }
                else{
                    List<Comments> userComments=ticket.getComments().stream().filter(comment ->!comment.getIsPrivate()).toList();
                    return ResponseStructureUtil.getOkResponse(userComments);
//                return  new ResponseEntity<>(new ResponseStructure<>(HttpStatus.OK.value(), userComments,"comments of this ticket is"),(HttpStatus.OK));
                }

            }
            return ResponseStructureUtil.getNotFoundResponse("User Not Found");




    }

}
