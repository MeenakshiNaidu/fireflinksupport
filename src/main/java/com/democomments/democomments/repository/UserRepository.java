package com.democomments.democomments.repository;

import com.democomments.democomments.models.Users;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<Users,String> {

    Users findByEmail(String email);
}
