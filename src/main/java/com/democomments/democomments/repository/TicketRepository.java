package com.democomments.democomments.repository;

import com.democomments.democomments.models.Tickets;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface TicketRepository extends MongoRepository<Tickets,String> {
}
