package com.democomments.democomments.util;

import com.democomments.democomments.payload.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseStructureUtil {

    public static ApiResponse apiResponse(){
        return new ApiResponse();
    }

    public static ResponseEntity<ApiResponse> getCreatedResponse(Object data){
        ApiResponse apiResponse=apiResponse();
        apiResponse.setHttpStatus(HttpStatus.CREATED);
        apiResponse.setStatusCode(HttpStatus.CREATED.value());
        apiResponse.setResponse(data);
        return ResponseEntity.status(apiResponse.getStatusCode()).body(apiResponse);
    }

    public static ResponseEntity<ApiResponse> getBadRequestResponse(Object response) {
        ApiResponse apiResponse = apiResponse();
        apiResponse.setHttpStatus(HttpStatus.BAD_REQUEST);
        apiResponse.setStatusCode(HttpStatus.BAD_REQUEST.value());
        apiResponse.setResponse(response);
        return ResponseEntity.status(apiResponse.getStatusCode()).body(apiResponse);
    }

    public static ResponseEntity<ApiResponse> getOkResponse(Object response) {
        ApiResponse apiResponse = apiResponse();
        apiResponse.setHttpStatus(HttpStatus.OK);
        apiResponse.setStatusCode(HttpStatus.OK.value());
        apiResponse.setResponse(response);
        return ResponseEntity.status(apiResponse.getStatusCode()).body(apiResponse);
    }

    public static ResponseEntity<ApiResponse> getConflictResponse(Object response)
    {
        ApiResponse apiResponse=apiResponse();
        apiResponse.setHttpStatus(HttpStatus.CONFLICT);
        apiResponse.setStatusCode(HttpStatus.CONFLICT.value());
        apiResponse.setResponse(response);
        return ResponseEntity.status(apiResponse.getStatusCode()).body(apiResponse);
    }

    public static ResponseEntity<ApiResponse> getNotFoundResponse(Object response){
        ApiResponse apiResponse=apiResponse();
        apiResponse.setResponse(response);
        apiResponse.setHttpStatus(HttpStatus.NOT_FOUND);
        apiResponse.setStatusCode(HttpStatus.OK.value());
        return ResponseEntity.status(apiResponse.getStatusCode()).body(apiResponse);
    }

    public static ResponseEntity<ApiResponse> getUnauthorisedResponse(Object response){
        ApiResponse apiResponse=apiResponse();
        apiResponse.setStatusCode(HttpStatus.UNAUTHORIZED.value());
        apiResponse.setResponse(HttpStatus.UNAUTHORIZED);
        apiResponse.setResponse(response);
        return ResponseEntity.status(apiResponse.getStatusCode()).body(apiResponse);
    }
}
