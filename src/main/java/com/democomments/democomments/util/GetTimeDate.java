package com.democomments.democomments.util;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class GetTimeDate {
    public static String getCurrentTime(){
        DateTimeFormatter format=DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        var date= ZonedDateTime.now();
        return date.format(format);
    }
}
